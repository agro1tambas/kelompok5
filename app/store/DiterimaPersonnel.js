Ext.define('kelompok5.store.DiterimaPersonnel', {
    extend: 'Ext.data.Store',
    storeId: 'diterimapersonnel',
    autoLoad: true,
    autoSync: true,

    alias: 'store.diterimapersonnel',

    fields: [
        'nik', 'nama', 'gender', 'alamat', 'email', 'phone', 'tempat_lahir', 'tggl_lahir', 'kewarganegaraan', 'pekerjaan', 'tggl_daftar', 'alasan'
    ],

    proxy: {
        type: 'jsonp',
        api: {
            read: "http://localhost/DPM_5/readDiterimaPersonnel.php",
            create: "http://localhost/DPM_5/createPersonnel.php"
        },
        reader: {
            type: 'json',
            rootProperty: 'items',
            messageProperty: 'error'
        }
    }
});
