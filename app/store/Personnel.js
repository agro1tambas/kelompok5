Ext.define('kelompok5.store.Personnel', {
    extend: 'Ext.data.Store',
    storeId: 'personnel',
    autoLoad: true,
    autoSync: true,

    alias: 'store.personnel',

    fields: [
        'nik', 'nama', 'gender', 'alamat', 'email', 'phone', 'tempat_lahir', 'tggl_lahir', 'kewarganegaraan', 'pekerjaan', 'tggl_daftar', 'alasan'
    ],

    proxy: {
        type: 'jsonp',
        api: {
            read: "http://localhost/DPM_5/readPersonnel.php",
            create: "http://localhost/DPM_5/createPersonnel.php"
        },
        reader: {
            type: 'json',
            rootProperty: 'items',
            messageProperty: 'error'
        }
    }
});
