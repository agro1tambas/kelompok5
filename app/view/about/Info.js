Ext.define('kelompok5.view.about.Info', {
    extend: 'Ext.Container',
    xtype: 'info',

    requires: [
        'Ext.carousel.Carousel'
    ],

    shadow: true,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    defaults: {
        cls: 'demo-solid-background',
        flex: 1
    },
    items: [{
        xtype: 'carousel',
        items: [{
            html: '<div style="background-color: #2c3e50; color: #ffffff;"><center><font size=10>Ketentuan</font></center></div><br><p style="text-align: left; margin: 10px;"><font size=5>* Pria / Wanita Pendidikan SMA, SMK, D1. D2. D3, D4. S1 & S2. S3, semua jurusan<br>* Pengalaman dalam bidangnya<br>* Memiliki kemampuan dan dapat berkomunikasi dengan baik<br>* Teliti, disiplin dan bertanggungjawab serta motivasi kerja tinggi<br>* Dapat bekerja secara individu maupun dalam team</font></p>'
        },
        {
            html: '<div style="background-color: #2c3e50; color: #ffffff;"><center><font size=10>Jam Kerja</font></center></div><br><p style="text-align: left; margin: 10px;"><font size=5>* Full Time: Senin S/D Sabtu (09:00 S/D 15:00 jam siang), <br>* Full Time: Senin S/D Sabtu (15:00 S/D 21:00 jam malam), <br>* Hari Minggu/Merah Libur (Non Shif/Lembur).</font></p>'
        }]
    }]
});