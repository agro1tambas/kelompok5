Ext.define('kelompok5.view.detail.Akun', {
    extend: 'Ext.form.Panel',
    xtype: 'akuns',

    title: 'Pendaftaran Karyawan Restoran ABC',

    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Number',
        'Ext.field.Spinner',
        'Ext.field.Password',
        'Ext.field.Email',
        'Ext.field.Url',
        'Ext.field.DatePicker',
        'Ext.field.Select',
        'Ext.field.Hidden',
        'Ext.field.Radio'
    ],
    shadow: true,
    cls: 'demo-solid-background',
    id: 'basicform',
    items: [
        {
            xtype: 'fieldset',
            id: 'fieldset1',
            title: 'Pendaftaran Karyawan',
            defaults: {
                labelWidth: '35%'
            },
            items: [
                {
                    xtype: 'textfield',
                    revealable: true,
                    name : 'nik',
                    label: 'NIK',
                    id: 'mynik',
                    placeHolder: 'Masukkan NIK anda',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'textfield',
                    name: 'name',
                    label: 'Nama',
                    id: 'mynama',
                    placeHolder: 'Masukkan nama anda',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'selectfield',
                    name: 'gender',
                    id: 'mygender',
                    label: 'Gender',
                    options: [
                        {
                            text: 'Pria',
                            value: 'pria'
                        },
                        {
                            text: 'Wanita',
                            value: 'wanita'
                        }
                    ]
                },
                {
                    xtype: 'textfield',
                    name: 'alamat',
                    label: 'Alamat',
                    id: 'myalamat',
                    placeHolder: 'Masukkan alamat anda',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'emailfield',
                    name: 'email',
                    label: 'Email',
                    id: 'myemail',
                    placeHolder: 'email-anda@email.com',
                    clearIcon: true
                },
                {
                    xtype: 'textfield',
                    name: 'phone',
                    label: 'Nomor Handphone',
                    id: 'myphone',
                    placeHolder: '08.. .... ....',
                    clearIcon: true
                },
                {
                    xtype: 'textfield',
                    name: 'tempat_lahir',
                    label: 'Tempat Lahir',
                    id: 'mytempat_lahir',
                    placeHolder: 'Masukkan tempat lahir anda',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'datepickerfield',
                    destroyPickerOnHide: true,
                    name: 'tggl_lahir',
                    label: 'Tanggal Lahir',
                    id: 'mytggl_lahir',
                    value: new Date(),
                    picker: {
                        yearFrom: 1990
                    }
                },
                {
                    xtype: 'textfield',
                    name: 'kewarganegaraan',
                    label: 'Kewarganegaraan',
                    id: 'mykewarganegaraan',
                    placeHolder: 'Masukkan kewarganegaraan anda',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'selectfield',
                    name: 'pekerjaan',
                    label: 'Pekerjaan',
                    id: 'mypekerjaan',
                    options: [
                        {
                            text: 'Koki',
                            value: 'koki'
                        },
                        {
                            text: 'Resepsonis',
                            value: 'resepsonis'
                        },
                        {
                            text: 'Satpam',
                            value: 'satpam'
                        },
                        {
                            text: 'Kasir',
                            value: 'kasir'
                        },
                        {
                            text: 'Delivery',
                            value: 'delivery'
                        }
                    ]
                },
                {
                    xtype: 'datepickerfield',
                    destroyPickerOnHide: true,
                    name: 'tggl_daftar',
                    label: 'Tanggal Daftar',
                    id: 'mytggl_daftar',
                    value: new Date(),
                    picker: {
                        yearFrom: 2020
                    }
                },
                {
                    xtype: 'textareafield',
                    name: 'alasan',
                    id: 'myalasan',
                    label: 'Alasan anda mengambil bidang ini',
                    placeHolder: 'Masukkan alasan anda',
                }
            ]
        },
        {
            xtype: 'container',
            defaults: {
                xtype: 'button',
                style: 'margin: 1em',
                flex: 1
            },
            layout: {
                type: 'hbox'
            },
            items: [
                {
                    text: 'Reset',
                    ui: 'action',
                    handler: function(){
                        Ext.getCmp('basicform').reset();
                    }
                },
                {
                    text: 'Daftar',
                    ui: 'confirm',
                    handler: 'onTambah'
                    /*handler: function(){
                        Ext.Msg.confirm('Confirm', 'Apakah data sudah Benar?', 'onConfirm', this);
                    },

                    onConfirm: function(choice){
                        if (choice === 'yes') {
                            Ext.toast('Pendaftaran Berhasil');
                            var mainView = Ext.getCmp('app-main');
                            mainView.setActiveItem(1);
                        }
                    }*/
                }
            ]
        }
    ]
});