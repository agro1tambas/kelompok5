/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('kelompok5.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    onItemSelected: function (sender, record) {
        var nama = record.data.nama;
        localStorage.setItem('nama', nama);
        Ext.toast(nama+ ' Sudah Terdaftar');
        console.log(record.data);
    },


    onDataPilih: function (sender, record) {
        var nama = record.data.nama;
        var status = record.data.status;
        localStorage.setItem('nama', nama);
        localStorage.setItem('status', status);
        Ext.toast(nama+ ' Sudah Terdaftar dan Sudah '+status );
        console.log(record.data);
    },

    onTambah: function(){
        Ext.Msg.confirm('Confirm', 'Apakah data sudah Benar?', 'onConfirm', this);

        nik = Ext.getCmp('mynik').getValue();
        nama = Ext.getCmp('mynama').getValue();
        gender = Ext.getCmp('mygender').getValue();
        alamat = Ext.getCmp('myalamat').getValue();
        email = Ext.getCmp('myemail').getValue();
        phone = Ext.getCmp('myphone').getValue();
        tempat_lahir = Ext.getCmp('mytempat_lahir').getValue();
        tggl_lahir = Ext.getCmp('mytggl_lahir').getValue();
        kewarganegaraan = Ext.getCmp('mykewarganegaraan').getValue();
        pekerjaan = Ext.getCmp('mypekerjaan').getValue();
        tggl_daftar = Ext.getCmp('mytggl_daftar').getValue();
        alasan = Ext.getCmp('myalasan').getValue();

        store = Ext.getStore('personnel');
        store.beginUpdate();
        store.insert(0, {'nik': nik, 'nama': nama, 'gender': gender, 'alamat': alamat, 'email': email, 'phone': phone, 'tempat_lahir': tempat_lahir, 'tggl_lahir': tggl_lahir, 'kewarganegaraan': kewarganegaraan, 'pekerjaan': pekerjaan, 'tggl_daftar': tggl_daftar, 'alasan': alasan});
        store.endUpdate();
    },

    onConfirm: function(choice){
        if (choice === 'yes') {
            Ext.toast('Pendaftaran Berhasil');
            var mainView = Ext.getCmp('app-main');
            mainView.setActiveItem(1);
        }
    }
});
