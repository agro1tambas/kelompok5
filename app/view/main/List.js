/**
 * This view is an example list of people.
 */
Ext.define('kelompok5.view.main.List', {
    extend: 'Ext.grid.Grid',
    xtype: 'mainlist',

    requires: [
        'kelompok5.store.Personnel'
    ],

    title: 'Terdaftar',

    /*store: {
        type: 'personnel'
    },*/

    bind: '{personnel}',

    viewModel: {
        stores: {
            personnel: {
                type: 'personnel'
            }
        }
    },

    columns: [
        { text: 'NIK',  dataIndex: 'nik', width: 180 },
        { text: 'Nama', dataIndex: 'nama', width: 160 },
        { text: 'Tanggal Daftar', dataIndex: 'tggl_daftar', width: 160 }
    ],

    listeners: {
        select: 'onItemSelected'
    }
});
