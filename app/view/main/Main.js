/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 *
 * TODO - Replace the content of this view to suit the needs of your application.
 */
Ext.define('kelompok5.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',
    id: 'app-main',

    requires: [
        'Ext.MessageBox',

        'kelompok5.view.main.MainController',
        'kelompok5.view.main.MainModel',
        'kelompok5.view.main.List',
        'kelompok5.view.detail.Akun',
        'Kelompok5.view.diterima.Status',
        'kelompok5.view.about.Info'
    ],

    controller: 'main',
    viewModel: 'main',

    defaults: {
        tab: {
            iconAlign: 'top'
        },
        styleHtmlContent: true
    },

    tabBarPosition: 'bottom',

    items: [
        {
            title: 'Daftar',
            iconCls: 'x-fa fa-users',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                xtype: 'akuns'
            }]
        },{
            title: 'Status',
            iconCls: 'x-fa fa-user-plus',
            layout: 'fit',
            items: [{
                xtype: 'status'
            }]
        },{
            title: 'Info',
            iconCls: 'x-fa fa-info-circle',
            layout: 'fit',
            items: [{
                xtype: 'info'
            }]
        }
    ]
});
