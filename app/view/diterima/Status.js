Ext.define('Kelompok5.view.diterima.Status', {
    extend: 'Ext.tab.Panel',
    xtype: 'status',

    shadow: true,
    cls: 'demo-solid-background',
    tabBar: {
        layout: {
            pack: 'center'
        }
    },
    activeTab: 1,
    defaults: {
        scrollable: true
    },
    items: [
        {
            title: 'Terdaftar',
            iconCls: 'x-fa fa-registered',
            layout: 'fit',
            items: [{
                xtype: 'mainlist'
            }]
        },
        {
            title: 'Diterima',
            iconCls: 'x-fa fa-check',
            layout: 'fit',
            items: [{
                xtype: 'diterima'
            }]
        },
    ]
});