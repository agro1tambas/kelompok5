/**
 * This view is an example list of people.
 */
Ext.define('kelompok5.view.diterima.Diterima', {
    extend: 'Ext.grid.Grid',
    xtype: 'diterima',

    requires: [
        'kelompok5.store.Personnel'
    ],

    title: 'Diterima',

    /*store: {
        type: 'personnel'
    },*/

    bind: '{personnel}',

    viewModel: {
        stores: {
            personnel: {
                type: 'personnel'
            }
        }
    },

    columns: [
        { text: 'NIK',  dataIndex: 'nik', width: 170 },
        { text: 'Nama', dataIndex: 'nama', width: 100 },
        { text: 'Bidang Pekerjaan', dataIndex: 'pekerjaan', width: 140 },
        { text: 'Status', dataIndex: 'status', width: 90}
    ],

    listeners: {
        select: 'onDataPilih'
    }
});
